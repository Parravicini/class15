"""
This is a template algorithm on Quantopian for you to adapt and fill in.
"""

def initialize(context): # the context is an augmented dict
    context.security = symbol('TSLA')
    # it tells the algo when to run. 
    # action.func is no defined yet. it will        # tell the algo what to do                                   
    
    schedule_function(func=action_func,
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_close()
                      )
def action_func(context, data):
    price_history = data.history(assets=context.security, fields='price',
                                 bar_count=10, frequency='1d')
    average_price= price_history.mean() #get the mean of these 10 days
    
    current_price = data.current(assets=context.security, fields='price')
    
    if data.can_trade(context.security):
        if current_price > average_price*1.03:
            order_target_percent(context.security,1)
        else:
            order_target_percent(context.security,0)
            
            
THIS IS THE OTHER WHICH IS AWESOME

"""
This is a template algorithm on Quantopian for you to adapt and fill in.
"""

def initialize(context): # the context is an augmented dict
    context.assets = sid(8554) #aapl, msft, spy
    # it tells the algo when to run. 
    # action.func is no defined yet. it will                                           # tell the algo what to do
    
    schedule_function(func=action_func,
                      date_rule=date_rules.month_start(days_offset=1),
                      time_rule=time_rules.market_open()
                      )
def action_func(context, data):
    volume_history = data.history(assets=context.assets, fields='volume',
                                 bar_count=3, frequency='1d')
    average_volume= volume_history.mean() #get the mean of these 3 days
    
    current_volume = data.current(assets=context.assets, fields='volume')
    
    if data.can_trade(context.assets):
      
        if current_volume < average_volume*1.1:
            order_target_percent(context.assets, 0.3)
        else:
            order_target_percent(context.assets, 0)

RAN FROM 2008 TO 2018: VERY LOW VOLATILITYYYYYYYYY AND DRAWDOWN(CONSIDER THE CRISIS)

Total Returns
33.25%
 
Benchmark Returns
125.53%
 
Alpha
0.00
 
Beta
0.29
 
Sharpe
0.51
 
Sortino
0.71
 
Volatility
0.06
 
Max Drawdown
-18.5%